# Changelog

All notable changes to this project will be documented in this file.

The format is based on [Keep a Changelog](https://keepachangelog.com/en/1.0.0/),
and this project adheres to [Semantic Versioning](https://semver.org/spec/v2.0.0.html).

## 4.2.1 - 2019-04-22

### Added

- Added GitHub Repo link to Navbar.

## 4.2.0 - 2019-04-13

### Added

- Now supporting Internet Explorer 11.

### Changed

- Changed persistent drawer to a temporary drawer.

## 4.1.1 - 2019-04-10

### Changed

- Updated react-redux to v7.0.1.

## 4.1.0 - 2019-03-31

### Added

- Polyfill fetch API.

### Changed

- Better handling of API server error.

### Removed

- Privacy View.

## 4.0.7 - 2019-03-29

### Changed

- Added a note on message board view.
- Updated deps.

## 4.0.6 - 2019-03-27

### Changed

- Added Content-Type header to calculation's form fetch POST request.

## 4.0.5 - 2019-03-27

### Changed

- Now browser won't automatically open when using `npm run start`.

### Fixed

- Fixed a bug on Calculation View introduced with the change from axios to fetch api.

## 4.0.4 - 2019-03-26

### Changed

- Updated react and react dom.

## 4.0.3 - 2019-03-21

### Added

- Added changelog.

## 4.0.2 - 2019-03-21

### Changed

- Now using double over single quotes.

### Removed

- Redundant bootstrap table css files.

## 4.0.1 - 2019-03-21

### Added

- Added documentation for util.js.

### Changed

- loadFromLocalStorage now returns undefined.
- Made Date more specific in categoriesActions.

## 4.0.0 - 2019-03-21

### Added

- CSS tables from purecss.
- date-fns dep.

### Changed

- Replaced axios dep with fetch API.
- Refactored out styled-components, node-sass, prop-types, and moment.js deps.
- Updated React Router to v5.

### Removed

- Removed axios, styled-components, node-sass, prop-types, and moment.js deps.

## 3.0.1 - 2019-03-08

### Changed

- Updated dependencies.

### 3.0.0 - 2019-03-02

### Added

- Implemented Material UI library and removed Material Design Lite.
- Added buildspec.yml.
- apple-touch-icon link
- Missing aria-labels.
- Added more informative errors in CalculationView.
- Save categories data to localstorage until stale.
- Added robots noindex tag in index.html.
- Fonts are now served locally.
- SCSS file via node-sass to scope bootstrap table css under `legacy-table` class.
- Added source-map-explorer.
- Added analyse to cmd.
- Enabled service worker.
- Added PageProgressBar.
- Removed ScrollLink in favour of ScrollContext.

### Changed

- manifest.json.
- Use shorthand return in funcs.
- Increased font weight on AppBar.
- Updated body CSS removing fonts.
- Moved derive functions into a util.js.
- Use undefined over null throughout the project.
- Refactored CalculationView for better code comprehension.
- Don't send request if same input is given for calc.
- Reduced transitionDelay in LoadingSpinner to 200ms.
- Moved images into assets folder.
- Reorganised components into `generic` folder.
- Improved visuals of calc and category views.
- Updated README with more information about MathFever API.

### Fixed

- Fixed issue where useEffect was not fired in CalculationView.
- Fixed CalculationView not showing error.

### Removed

- Removed redundant CSS class in index.html.
- Removed parse.
- Removed use of Material Design Lite.
- Removed redundant `th` tags in ConversionTableView.
- Removed version No. from README.

### 2.0.0 - 2019-02-17

### Added

- React Hooks! ✨
- Lazy loaded routes ⚡️

### Changed

- Using React.Fragment for a shallower DOM tree ⚡
- Updated decencies ⬆

### 1.0.1 - 2018-12-18

### Changed

- Refactored code for better readability.
- Calculation page is slightly more resilient to error.

### 1.0.0 - 2018-12-14

### Added

- Added parse.go.

### Changed

- Code refactor.
- MDL does not need to be rendered before react bundle now.
- Scroll to top happens instantly on scroll link press.

### 0.7.1 - 2018-12-3

### Added

- This frontend is now migrated to a AWS S3 Bucket and its backend has been migrated from a
  standalone Golang application to a AWS Serverless Golang backend.

### Changed

- ScrollLink handling has been improved.
