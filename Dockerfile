### STAGE 1: Build ###
FROM node:14.14.0-alpine AS build
WORKDIR /home/node/app
COPY ./dev/package.json ./dev/package-lock.json ./
RUN npm install --quiet
COPY ./dev .
RUN npm run build --quiet

### STAGE 2: Run ###
FROM caddy:2.1.1-alpine
COPY deploy/Caddyfile /etc/caddy
COPY --from=build /home/node/app/build /var/www/html
EXPOSE 8001
