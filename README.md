# MathFever Frontend

The client side component of MathFever. A website where users can find mathematical proof and
answers to common calculation problems. The repository of the API the application consumes can be
found here: [mathfever-api](https://gitlab.com/ludw1gj/mathfever-api).

This repo and [mathfever-api](https://gitlab.com/ludw1gj/mathfever-api) were both derived from
[mathfever-go](https://gitlab.com/ludw1gj/mathfever-go), which is the full golang implementation.
Here, I have uncoupled the Frontend and Backend of the application, by rewriting the frontend in
React as an SPA, and the backend as an API based on serverless cloud computing on AWS.

## Setup Notes

```bash
# To run development server
npm run start

# Development server running at http://localhost:3000

# To build
npm run build
```

## API

```bash
# GET
/api/categories
/api/category/{category}
/api/calculation/{calculation}

# POST
/api/calculation/{calculation}
```

If you POST empty or incorrect data to /api/calculation/{calculation} the server will reply with a JSON error with details about the correct input. For example sending invalid POST data to /api/calculation/binary-to-decimal the server will reply with:

```json
{
  "error": "invalid json: json must be {\"binary\": string}"
}
```


### Docker Setup

```
docker build -t mathfever-react-image .
docker run --name mathfever-react-container -d -p 8001:8001 mathfever-react-image
```
