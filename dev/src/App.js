import React, {useEffect} from "react";
import {BrowserRouter} from "react-router-dom";
import {connect} from "react-redux";
import {ScrollContext} from "react-router-scroll-4";

import {getCategories} from "./store/actions/categoriesActions";
import AppRouter from "./router";
import AppContainer from "./components/AppContainer";

/** The main component. */
function App({getCategories}) {
  useEffect(() => {
    getCategories();
  }, []);

  return (
    <BrowserRouter>
      <ScrollContext>
        <AppContainer>
          <AppRouter/>
        </AppContainer>
      </ScrollContext>
    </BrowserRouter>
  );
}

const mapDispatchToProps = dispatch => {
  return {
    getCategories: () => dispatch(getCategories()),
  };
};

export default connect(
  undefined,
  mapDispatchToProps
)(App);
