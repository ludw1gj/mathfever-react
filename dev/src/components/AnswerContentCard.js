import React from "react";

import PaperSheet from "./generic/PaperSheet";
import {withStyles} from "@material-ui/styles";

const useStyles = theme => ({
  paper: {
    zIndex: 1,
    position: "relative",
  },
});

class AnswerContentCard extends React.PureComponent {
  render() {
    const {answer, error, style, classes} = this.props;

    const content = () => {
      if (error) {
        return <p>{error}</p>;
      }
      if (answer) {
        return <div dangerouslySetInnerHTML={{__html: answer}}/>;
      }
      return <div/>;
    };

    return (
      <div style={style}>
        <PaperSheet className={classes.paper}>{content()}</PaperSheet>
      </div>
    );
  }
}

export default withStyles(useStyles)(AnswerContentCard)
