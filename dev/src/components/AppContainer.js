import React, {useState} from "react";
import {CssBaseline, Divider, withStyles} from "@material-ui/core";
import LoadingBar from "react-redux-loading-bar";

import AppNavBar from "./AppNavBar";
import Content from "./generic/Content";
import Footer from "./Footer";

const styles = theme => ({
  root: {
    display: "flex",
  },
  content: {
    flexGrow: 1,
    padding: theme.spacing(3),
  },
  navSpacer: {
    ...theme.mixins.toolbar,
  },
});

function AppContainer(props) {
  const [drawerActive, setDrawerActive] = useState(false);

  const handleDrawerOpen = () => {
    setDrawerActive(true);
  };

  const handleDrawerClose = () => {
    setDrawerActive(false);
  };

  const {classes} = props;

  return (
    <div className={classes.root}>
      <CssBaseline/>
      <LoadingBar
        scope="pageProgress"
        style={{
          zIndex: 2000,
          position: "fixed",
          top: 0,
          backgroundColor: "white",
          opacity: 0.5,
        }}
      />
      <AppNavBar
        drawerActive={drawerActive}
        handleDrawerOpen={handleDrawerOpen}
        handleDrawerClose={handleDrawerClose}
      />
      <div className={classes.content}>
        <div className={classes.navSpacer}/>
        <Content>{props.children}</Content>
        <Divider variant="middle"/>
        <Footer/>
      </div>
    </div>
  );
}

export default withStyles(styles, {withTheme: true})(AppContainer);
