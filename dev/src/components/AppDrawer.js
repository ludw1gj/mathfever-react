import React from "react";
import {Link} from "react-router-dom";

import {Drawer, List, ListItem, ListItemIcon, ListItemText, withStyles} from "@material-ui/core";

import {DRAWER_WIDTH, routes} from "../services/config";

const DrawerLink = ({children, to, ...rest}) => {
  return (
    <Link to={to} {...rest} style={{textDecoration: "none", color: "inherit"}}>
      {children}
    </Link>
  );
};

const styles = {
  list: {
    width: DRAWER_WIDTH + 10,
  },
};

function AppDrawer(props) {
  const {classes} = props;

  const sideList = (
    <div className={classes.list}>
      <List>
        {routes.map(route => (
          <ListItem
            button
            key={route.name + "-link"}
            to={route.url}
            component={DrawerLink}
            onClick={props.handleDrawerClose}>
            <ListItemIcon>
              <route.icon/>
            </ListItemIcon>
            <ListItemText primary={route.name}/>
          </ListItem>
        ))}
      </List>
    </div>
  );

  return (
    <Drawer
      className={classes.drawer}
      anchor="left"
      open={props.active}
      onClose={props.handleDrawerClose}
      classes={{
        paper: classes.drawerPaper,
      }}>
      {sideList}
    </Drawer>
  );
}

export default withStyles(styles)(AppDrawer);
