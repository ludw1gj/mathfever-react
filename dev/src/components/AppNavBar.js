import React from "react";
import {AppBar, Button, IconButton, Toolbar, Typography, withStyles} from "@material-ui/core";
import useMediaQuery from "@material-ui/core/useMediaQuery";
import {Menu} from "@material-ui/icons";
import {Link} from "react-router-dom";

import GitLabLogo from "../assets/images/gitlab_logo.svg";
import {routes} from "../services/config";
import AppDrawer from "./AppDrawer";

const styles = {
  grow: {
    flexGrow: 1,
  },
  menuButton: {
    marginRight: 20,
  },
  link: {
    textDecoration: "none",
    color: "inherit",
    fontWeight: 500,
  },
  gitlabIcon: {
    height: "24px",
  },
};

function AppNavBar(props) {
  const {classes, drawerActive} = props;

  const isSmall = useMediaQuery("(max-width:625px)");

  const displayButtons = () => {
    return (
      <>
        {routes.map(route => (
          <Button
            className={classes.link}
            component={Link}
            to={route.url}
            key={route.name + "-link"}
            aria-label={route.name}
            color="inherit">
            {route.name}
          </Button>
        ))}
        <Button
          className={classes.link}
          aria-label={"GitHub Repo"}
          color="inherit"
          href="https://gitlab.com/ludw1gj/mathfever-react">
          <img alt={"GitHub Repo"} src={GitLabLogo} className={classes.gitlabIcon}/>
        </Button>
      </>
    );
  };

  return (
    <>
      <AppBar position="fixed">
        <Toolbar>
          <IconButton
            className={classes.menuButton}
            color="inherit"
            aria-label="Menu"
            onClick={props.handleDrawerOpen}>
            <Menu/>
          </IconButton>
          <Typography variant="h6" color="inherit" className={classes.grow} noWrap>
            MathFever
          </Typography>

          {isSmall ? undefined : displayButtons()}
        </Toolbar>
      </AppBar>

      <AppDrawer active={drawerActive} handleDrawerClose={props.handleDrawerClose}/>
    </>
  );
}

export default withStyles(styles)(AppNavBar);
