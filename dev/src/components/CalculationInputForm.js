import React from "react";
import {Button, TextField} from "@material-ui/core";
import {makeStyles} from "@material-ui/styles";

const useStyles = makeStyles({
  container: {
    display: "flex",
    flexWrap: "wrap",
    alignItems: "center",
    alignContent: "center",
    marginLeft: "5%",
  },
  field: {
    marginRight: "2em",
  },
  button: {},
});

function CalculationInputForm({calculation, onSubmit}) {
  const classes = useStyles();
  return (
    <form noValidate autoComplete="off" className={classes.container} onSubmit={onSubmit}>
      {calculation.inputInfo.map(inputField => {
        return (
          <TextField
            key={inputField.tag}
            name={inputField.tag}
            label={inputField.name}
            className={classes.field}
            margin="normal"
          />
        );
      })}

      <Button className={classes.button} variant="contained" type="submit" aria-label="Calculate">
        Calculate
      </Button>
    </form>
  );
}

export default CalculationInputForm;
