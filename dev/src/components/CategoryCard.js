import React from "react";
import {Button, Card, CardActionArea, CardContent, CardMedia, List, ListItem,} from "@material-ui/core";
import {makeStyles} from "@material-ui/styles";
import {Link} from "react-router-dom";

const useStyles = makeStyles({
  card: {
    minWidth: 300,
    maxWidth: 375,
    margin: "auto",
  },
  media: {
    height: 140,
  },
  listItem: {
    textDecoration: "none",
    color: "#696969",
  },
});

function CategoryCard(props) {
  const {category, image} = props;
  const classes = useStyles();

  const listItems = category.calculations.map(calc => {
    return (
      <ListItem
        dense
        button
        disableRipple
        aria-label={calc.name}
        key={calc.slug}
        component={Link}
        className={classes.listItem}
        to={`/category/${category.slug}/${calc.slug}`}>
        {calc.name}
      </ListItem>
    );
  });

  return (
    <Card className={classes.card}>
      <CardActionArea component={Link} to={`/category/${category.slug}`}>
        <CardMedia className={classes.media} image={image} title={category.name}/>
      </CardActionArea>
      <CardContent>
        <Button
          disableRipple
          style={{fontSize: "1em"}}
          component={Link}
          aria-label={category.name}
          to={`/category/${category.slug}`}>
          {category.name}
        </Button>
        <List>{listItems}</List>
      </CardContent>
    </Card>
  );
}

export default CategoryCard;
