import React from "react";
import {Divider, List, ListItem, Typography} from "@material-ui/core";
import {makeStyles} from "@material-ui/styles";
import {Link} from "react-router-dom";

import MetaDescription from "./generic/MetaDescription";
import PaperSheet from "./generic/PaperSheet";
import PageHeading from "./generic/PageHeading";

const useStyles = makeStyles({
  list: {
    maxWidth: "300px",
  },
  listItem: {
    color: "rgb(255, 64, 129)",
  },
  hr: {
    margin: "1em 0",
  },
});

function CategoryContent({category}) {
  const classes = useStyles();
  return (
    <>
      <PageHeading>{category.name}</PageHeading>
      <MetaDescription>{category.description}</MetaDescription>

      <PaperSheet>
        <Typography variant="h5" component="h4" gutterBottom>
          {category.name} Calculations:
        </Typography>
        <List dense className={classes.list}>
          {category.calculations.map(calc => (
            <ListItem
              button
              disableRipple
              className={classes.listItem}
              key={calc.slug}
              component={Link}
              aria-label={calc.name}
              to={`/category/${category.slug}/${calc.slug}`}>
              {calc.name}
            </ListItem>
          ))}
        </List>

        {category.calculations.map(calc => (
          <React.Fragment key={calc.slug}>
            <Divider variant="fullWidth" className={classes.hr}/>
            <Typography variant="h5" component="h4" gutterBottom>
              {calc.name}
            </Typography>
            <div dangerouslySetInnerHTML={{__html: calc.example}}/>
          </React.Fragment>
        ))}
      </PaperSheet>
    </>
  );
}

export default CategoryContent;
