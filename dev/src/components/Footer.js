import React from "react";
import {makeStyles} from "@material-ui/styles";
import {Button} from "@material-ui/core";
import {Link} from "react-router-dom";

import SmallCaps from "./generic/SmallCaps";

const useStyles = makeStyles({
  container: {
    marginTop: "1em",
    display: "flex",
    flexDirection: "row",
    justifyContent: "flex-start",
    alignItems: "center",
    width: "100%",
  },
  item: {
    marginRight: "0.5em",
  },
});

function LinkButton(props) {
  const classes = useStyles();
  return <Button className={classes.item} component={Link} {...props} />;
}

/** Footer component is a footer with MDL specific classes. */
function Footer() {
  const classes = useStyles();
  return (
    <div className={classes.container}>
      <SmallCaps className={classes.item}>&#169; 2019 MathFever</SmallCaps>
      <LinkButton to={"/help"} aria-label="Help">
        Help
      </LinkButton>
      <LinkButton to={"/terms"} aria-label="Terms of Use">
        Terms of Use
      </LinkButton>
    </div>
  );
}

export default Footer;
