import React from "react";
import MetaDescription from "./generic/MetaDescription";

/** GenericDocMetaDesc component sets the document's meta description with a default value. */
function GenericDocMetaDesc() {
  return (
    <MetaDescription>
      MathFever - A website where users can find mathematical proof and answers to common math
      problems, with values of their choosing.
    </MetaDescription>
  );
}

export default GenericDocMetaDesc;
