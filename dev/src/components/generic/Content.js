import React from "react";

/** Content component which the web app's main content sits in. Minus the Navbar and Footer.  */
const Content = ({children, ...rest}) => {
  return (
    <div {...rest} style={{minHeight: "90vh", marginBottom: "1.5em"}}>
      {children}
    </div>
  );
};

export default Content;
