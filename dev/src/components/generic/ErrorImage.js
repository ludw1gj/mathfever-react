import React from "react";

/** ErrorImage component is a styled img tag for use of error images.  */
const ErrorImage = ({alt, ...rest}) => {
  return <img {...rest} alt={alt} style={{width: "100%", borderRadius: "15px"}}/>;
};

export default ErrorImage;
