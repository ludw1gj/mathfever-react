import React from "react";
import {CircularProgress, Fade} from "@material-ui/core";
import {makeStyles} from "@material-ui/styles";

const useStyles = makeStyles({
  container: {
    paddingTop: "3em",
    margin: "auto",
    textAlign: "center",
  },
});

function LoadingSpinner() {
  const classes = useStyles();
  return (
    <Fade
      in={true}
      timeout={{enter: 500, exit: 0}}
      style={{
        transitionDelay: "200ms",
      }}>
      <div className={classes.container}>
        <CircularProgress disableShrink/>
      </div>
    </Fade>
  );
}

export default LoadingSpinner;
