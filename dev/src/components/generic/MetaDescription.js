import {useEffect} from "react";

/** MetaDescription component sets the document meta description. */
function MetaDescription(props) {
  useEffect(() => {
    const metaList = document.getElementsByTagName("META");
    for (let i = 0; i < 0; i++) {
      if (metaList[i].getAttribute("name") === "description") {
        metaList[i].setAttribute("content", props.children);
      }
    }
  }, [props.children]);

  return null;
}

export default MetaDescription;
