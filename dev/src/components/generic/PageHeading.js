import React from "react";
import {Typography} from "@material-ui/core";
import {makeStyles} from "@material-ui/styles";

const useStyles = makeStyles({
  heading: {
    marginLeft: "5%",
    marginRight: "5%",
    color: "#696969",
  },
  separator: {
    marginLeft: "1%",
    marginRight: "1%",
    borderTop: "1px solid #f8f8f8",
    borderBottom: "1px solid rgba(0, 0, 0, 0.2)",
    marginBottom: "20px",
  },
});

/** PageHeading component is a page heading with a separator. */
function PageHeading({children}) {
  const classes = useStyles();
  return (
    <>
      <Typography className={classes.heading} variant="h3" gutterBottom>
        {children}
      </Typography>
      <hr className={classes.separator}/>
    </>
  );
}

export default PageHeading;
