import {useLayoutEffect} from "react";
import {connect} from "react-redux";
import {hideLoading, showLoading} from "react-redux-loading-bar";

function PageProgressBar({scope, showLoading, hideLoading}) {
  useLayoutEffect(() => {
    showLoading(scope);
    return () => {
      hideLoading(scope);
    };
  }, []);
  return null;
}

const mapDispatchToProps = dispatch => {
  return {
    showLoading: scope => dispatch(showLoading(scope)),
    hideLoading: scope => dispatch(hideLoading(scope)),
  };
};

export default connect(
  undefined,
  mapDispatchToProps
)(PageProgressBar);
