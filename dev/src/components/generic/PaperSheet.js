import React from "react";
import {withStyles} from "@material-ui/core";
import Paper from "@material-ui/core/Paper";

const styles = theme => ({
  container: {
    display: "flex",
    justifyContent: "center",
  },
  innerContainer: {
    paddingLeft: "5%",
    paddingRight: "5%",
  },
  root: {
    ...theme.mixins.gutters(),
    paddingTop: theme.spacing(4),
    paddingBottom: theme.spacing(4),
    flex: "0 1 900px",
  },
});

function PaperSheet(props) {
  const {classes} = props;

  return (
    <div className={classes.container}>
      <Paper className={classes.root} elevation={1}>
        <div className={classes.innerContainer}>{props.children}</div>
      </Paper>
    </div>
  );
}

export default withStyles(styles)(PaperSheet);
