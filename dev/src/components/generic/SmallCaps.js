import React from "react";

const styles = {
  smallCaps: {
    fontVariant: "small-caps",
  },
};

/** SmallCaps component is a styled span. */
function SmallCaps(props) {
  return (
    <span {...props} style={styles.smallCaps}>
      {props.children}
    </span>
  );
}

export default SmallCaps;
