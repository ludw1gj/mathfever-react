import "react-app-polyfill/ie11";
import "core-js/features/map";

import React from "react";
import ReactDOM from "react-dom";
import {applyMiddleware, createStore} from "redux";
import reduxThunk from "redux-thunk";
import {Provider} from "react-redux";

import App from "./App";
import rootReducer from "./store/reducers/rootReducer";
import * as serviceWorker from "./serviceWorker";

import "purecss/build/tables-min.css"; // NOTE: used for css tables
import "./index.css";

// The redux store.
const store = createStore(rootReducer, applyMiddleware(reduxThunk));

ReactDOM.render(
  <Provider store={store}>
    <App/>
  </Provider>,
  document.getElementById("root")
);

// If you want your app to work offline and load faster, you can change
// unregister() to register() below. Note this comes with some pitfalls.
// Learn more about service workers: http://bit.ly/CRA-PWA
serviceWorker.unregister();
