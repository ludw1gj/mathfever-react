import React, {lazy, Suspense} from "react";
import {Route, Switch} from "react-router-dom";
import PageProgressBar from "./components/generic/PageProgressBar";

const NotFoundView = lazy(() => import("./views/NotFoundView"));
const HomeView = lazy(() => import("./views/HomeView"));
const AboutView = lazy(() => import("./views/AboutView"));
const HelpView = lazy(() => import("./views/HelpView"));
const TermsView = lazy(() => import("./views/TermsView"));
const MessageBoardView = lazy(() => import("./views/MessageBoardView"));
const ConversionTableView = lazy(() => import("./views/ConversionTableView"));
const CategoryView = lazy(() => import("./views/CategoryView"));
const CalculationView = lazy(() => import("./views/CalculationView"));

/** AppRouter component handles routing. */
const AppRouter = () => {
  return (
    <Suspense fallback={<PageProgressBar scope={"pageProgress"}/>}>
      <Switch>
        <Route exact path="/" component={HomeView}/>
        <Route exact path="/about" component={AboutView}/>
        <Route exact path="/help" component={HelpView}/>
        <Route exact path="/terms" component={TermsView}/>
        <Route exact path="/message-board" component={MessageBoardView}/>
        <Route exact path="/category/networking/conversion-table" component={ConversionTableView}/>
        <Route exact path="/category/:categorySlug" component={CategoryView}/>
        <Route exact path="/category/:categorySlug/:calculationSlug" component={CalculationView}/>

        <Route path="*" component={NotFoundView}/>
      </Switch>
    </Suspense>
  );
};

export default AppRouter;
