import {DescriptionOutlined, Forum, HelpOutline, Home} from "@material-ui/icons";

export const routes = [
  {name: "Home", url: "/", icon: Home},
  {name: "Message Board", url: "/message-board", icon: Forum},
  {name: "About", url: "/about", icon: DescriptionOutlined},
  {name: "Help", url: "/help", icon: HelpOutline},
];

export const DRAWER_WIDTH = 240;
