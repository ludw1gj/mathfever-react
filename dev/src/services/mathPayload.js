/**
 * Create serialized math form if form values are valid (binary, decimal, hex).
 * @param {HTMLFormElement} htmlForm - The form to serialize.
 * @param {Function} onError - The function to execute if form is invalid.
 *
 * @returns {object} - A JSON string.
 */
export const createMathPayload = (htmlForm, onError) => {
  const payload = createObjFromHTMLForm(htmlForm);
  for (const key in payload) {
    payload[key] = payload[key].replace(/\s/g, ""); // remove whitespace
    if (!validateValue(payload[key], key, onError)) {
      return undefined;
    }
    parseValue(key, payload);
  }
  return JSON.stringify(payload);
};

/**
 * Converts HTML Form in js object.
 * @param {HTMLFormElement} htmlForm - The form to serialize.
 * @returns {object} - Serialized form obj.
 */
const createObjFromHTMLForm = htmlForm => {
  const elements = htmlForm.elements;
  const obj = {}; // { [k: string]: any }

  for (let i = 0; i < elements.length; i += 1) {
    /** @type {object} */
    const element = elements[i];
    const type = element.type;
    const name = element.name;
    const value = element.value;

    switch (type) {
      case "hidden":
      case "text":
        obj[name] = value;
        break;
      default:
        break;
    }
  }
  return obj;
};

/**
 * Check if value in serialized form is valid.
 * @param {string} value - A serialize form value.
 * @param {string} key - The key of the value to validate.
 * @param {Function} onError - The function to execute if key's value is invalid.
 * @returns {boolean} - Returns true if key and key's value is valid.
 */
const validateValue = (value, key, onError) => {
  if (value === "") {
    onError("Please enter a valid input.");
    return false;
  }

  switch (key) {
    case "binary":
      if (!isValidBinary(value)) {
        onError("Please enter a valid binary number, and is not over 64 characters in length.");
        return false;
      }
      break;
    case "decimal":
      if (!isValidDecimal(value)) {
        onError("Please enter a valid decimal number, and is not over 999,999,999,999.");
        return false;
      }
      break;
    case "hexadecimal":
      if (!isValidHexadecimal(value)) {
        onError(
          "Please enter a valid hexadecimal number, and is not over 64 characters in length."
        );
        return false;
      }
      break;
    default:
      if (!isValidDecimal(value)) {
        onError("Please enter a valid decimal number, and is not over 999,999,999,999.");
        return false;
      }
  }
  return true;
};

/**
 * Parse value in serialized form.
 * @param {string} key - The key of the value to parse.
 * @param {object} serializedForm - The serialized form.
 */
const parseValue = (key, serializedForm) => {
  switch (key) {
    case "binary":
      // leave value as binary string
      break;
    case "decimal":
      // value must be an integer string not a float string
      serializedForm[key] = parseInt(serializedForm[key]);
      break;
    case "hexadecimal":
      serializedForm[key] = serializedForm[key].toUpperCase();
      break;
    default:
      // percentages and total-surface-area category require a float input
      const currentPageURL = window.location.pathname;
      if (
        currentPageURL.indexOf("percentages") !== -1 ||
        currentPageURL.indexOf("total-surface-area") !== -1
      ) {
        serializedForm[key] = parseFloat(serializedForm[key]);
      } else {
        // input is for a int route such as numbers category
        serializedForm[key] = parseInt(serializedForm[key]);
      }
  }
};

/**
 * Check if binary value is valid.
 * @param {string} binary
 * @returns {boolean}
 */
const isValidBinary = binary => {
  if (binary.length > 64) {
    return false;
  }
  for (let i = 0; i < binary.length; i++) {
    if (binary[i] !== "0" && binary[i] !== "1") {
      return false;
    }
  }
  return true;
};

/**
 * Check if decimal value is valid.
 * @param {string | number} decimal
 * @returns {boolean}
 */
const isValidDecimal = decimal => {
  return !isNaN(Number(decimal.toString())) && decimal < 999999999999;
};

/**
 * Check if hexadecimal value is valid.
 * @param {string} hexadecimal
 * @returns {boolean}
 */
const isValidHexadecimal = hexadecimal => {
  return /^[A-F0-9]+$/.test(hexadecimal.toUpperCase()) && hexadecimal.length < 64;
};
