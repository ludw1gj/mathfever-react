/** Get categories from API. */
import {apiURL} from "../appConfig";

export const getCategories = () => {
  return fetch(`${apiURL}categories`, {
    method: "GET",
  });
};

/**
 * Get calculation output from API.
 * @param {string} calculationSlug - The slug of the calculation to get data for.
 * @param {string} payload - A JSON string payload.
 * @returns {Promise}
 */
export const getCalculationResult = (calculationSlug, payload) => {
  return new Promise((resolve, reject) => {
    fetch(`${apiURL}calculation/${calculationSlug}`, {
      method: "POST",
      body: payload,
      headers: {
        "Content-Type": "application/json",
      },
    })
      .then(resp => {
        resolve(resp);
      })
      .catch(err => {
        reject(err);
      });
  });
};
