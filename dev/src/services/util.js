/** Get specific category from categories data.
 *
 * @param {Object} categories - the categories data.
 * @param {Object} categorySlug - the slug of category of calculation to find.
 *
 * @returns {Object | undefined} - the derived category, undefined if none found.
 */
export const deriveCategory = (categories, categorySlug) => {
  const category = categories.filter(category => category.slug === categorySlug);
  if (category.length !== 1) {
    return undefined;
  }
  return category[0];
};

/** Get specific calculation from categories data.
 *
 * @param {Object} categories - the categories data.
 * @param {Object} categorySlug - the slug of category of calculation to find.
 * @param {Object} calculationSlug - whether to JSON.stringify the value.
 *
 * @returns {Object | undefined} - the derived calculation, undefined if none found.
 */
export const deriveCalculation = (categories, categorySlug, calculationSlug) => {
  const category = categories.filter(category => category.slug === categorySlug);
  if (category.length !== 1) {
    return undefined;
  }

  const calculation = category[0].calculations.filter(calc => {
    return calc.slug === calculationSlug;
  });
  if (calculation.length !== 1) {
    return undefined;
  }
  return calculation[0];
};

/** Save data to local storage.
 *
 * @param {string} key - the key identifier.
 * @param {Object} value - the data to store.
 * @param {Object} parse - whether to JSON.stringify the value.
 */
export const saveToLocalStorage = (key, value, parse) => {
  localStorage.setItem(key, parse ? JSON.stringify(value) : value);
};

/** Load data from local storage.
 *
 * @param {string} key - the key identifier of the data to retrieve.
 * @param {Object} parse - whether to JSON.stringify the item returned.
 *
 * @returns {Object | undefined} - value retrieved.
 */
export const loadFromLocalStorage = (key, parse) => {
  const item = localStorage.getItem(key);
  if (item) {
    return parse ? JSON.parse(item) : item;
  } else {
    return undefined;
  }
};
