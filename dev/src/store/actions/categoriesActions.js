import {differenceInDays, format, parse} from "date-fns";

import {GET_CATEGORIES, GET_CATEGORIES_ERROR} from "./types";
import {getCategories as queryCategories} from "../../services/query";
import {loadFromLocalStorage, saveToLocalStorage} from "../../services/util";

const dateFormat = "yyyy-MM-dd'T'HH:mm:ss.SSSxxx";

/** Get categories. */
export const getCategories = () => {
  return (dispatch, _) => {
    const currentDate = format(Date.now(), dateFormat);

    const dateSaved = loadFromLocalStorage("categoriesDataDate", false);
    const result = dateSaved ? differenceInDays(parse(currentDate, dateFormat, new Date()), parse(dateSaved, dateFormat, new Date())) : 0;
    if (result < 7) {
      const data = loadFromLocalStorage("categoriesData", true);
      if (data) {
        dispatch({type: GET_CATEGORIES, payload: data});
        return;
      }
    }

    queryCategories()
      .then(resp => {
        resp.json().then(data => {
          saveToLocalStorage("categoriesData", data, true);
          saveToLocalStorage("categoriesDataDate", currentDate, false);
          dispatch({type: GET_CATEGORIES, payload: data});
        });
      })
      .catch(err => {
        const error = err.response ? err.response : err.message;
        dispatch({type: GET_CATEGORIES_ERROR, payload: error});
      });
  };
};
