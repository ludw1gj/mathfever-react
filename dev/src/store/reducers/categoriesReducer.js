import {GET_CATEGORIES, GET_CATEGORIES_ERROR} from "../actions/types";

const initState = {
  data: undefined,
  error: undefined,
};

/** Reducer containing categories data. */
export const categoriesReducer = (state = initState, action) => {
  switch (action.type) {
    case GET_CATEGORIES:
      return {
        data: action.payload,
        error: undefined,
      };

    case GET_CATEGORIES_ERROR:
      return {
        data: undefined,
        error: action.payload,
      };

    default:
      return state;
  }
};
