import {combineReducers} from "redux";
import {loadingBarReducer} from "react-redux-loading-bar";

import {categoriesReducer} from "./categoriesReducer";

/** The root reducer. */
const rootReducer = combineReducers({
  categories: categoriesReducer,
  loadingBar: loadingBarReducer,
});

export default rootReducer;
