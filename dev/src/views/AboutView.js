import React from "react";
import {Typography} from "@material-ui/core";
import {Link} from "react-router-dom";

import DocumentTitle from "../components/generic/DocumentTitle";
import PaperSheet from "../components/generic/PaperSheet";
import SmallCaps from "../components/generic/SmallCaps";
import PageHeading from "../components/generic/PageHeading";
import GenericDocMetaDesc from "../components/GenericDocMetaDesc";

const styles = {
  link: {
    color: "rgb(255,64,129)",
  },
};

/** About page. */
function AboutView() {
  return (
    <>
      <DocumentTitle>About - MathFever</DocumentTitle>
      <GenericDocMetaDesc/>

      <PageHeading>About</PageHeading>

      <PaperSheet>
        <Typography variant="h5" component="h3" gutterBottom>
          <strong>Why, hello there!</strong>
        </Typography>
        <Typography paragraph>
          This website <SmallCaps>MathFever</SmallCaps> aims to help you do a dodgy job on your
          homework. A lone admin built this site so he could help people spend less time on their
          homework and more time out with friends. Users can find mathematical proof and answers to
          common math problems, with values of their choosing. Math problem such as Highest/Lowest
          Common Denominator, Volume/Total Surface Area of a handful of different shapes,
          Conversions for the field of Networking (Binary, Decimal, and Hexadecimal), and more. The
          admin hopes you find this site a somewhat bit useful.
        </Typography>
        <Typography paragraph>
          If you see any bugs, report them and they will be squashed! If you want to request any
          features or have ideas for improvement don't hesitate to give the admin a yell, he likes
          to program.
        </Typography>

        <Typography variant="h5" component="h3" gutterBottom>
          <strong>Contact</strong>
        </Typography>

        <Typography paragraph>
          Post a comment on the{" "}
          <Link style={styles.link} to={"/message-board"}>
            message board.
          </Link>
        </Typography>
      </PaperSheet>
    </>
  );
}

export default AboutView;
