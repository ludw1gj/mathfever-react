import React, {useEffect, useState} from "react";
import {connect} from "react-redux";
import {Link} from "react-router-dom";
import {Divider, Fab, Slide} from "@material-ui/core";
import {makeStyles} from "@material-ui/styles";

import ErrorView from "./ErrorView";
import LoadingSpinner from "../components/generic/LoadingSpinner";
import DocumentTitle from "../components/generic/DocumentTitle";
import MetaDescription from "../components/generic/MetaDescription";
import {getCalculationResult} from "../services/query";
import {createMathPayload} from "../services/mathPayload";
import AnswerContentCard from "../components/AnswerContentCard";
import CalculationInputForm from "../components/CalculationInputForm";
import {deriveCalculation} from "../services/util";

const useStyles = makeStyles({
  breadcrumb: {
    display: "flex",
    flexDirection: "row",
  },
  categoryHeading: {
    marginLeft: "5%",
  },
  calcHeading: {
    marginLeft: "5%",
    fontFamily: "sans-serif",
    fontWeight: 200,
  },
  separator: {
    marginTop: "1em",
    marginBottom: "1.5em",
  },
});

function processCalculationProps(categoriesData, categorySlug, calculationSlug, onError) {
  if (!categoriesData) {
    return undefined;
  }
  const calculation = deriveCalculation(categoriesData, categorySlug, calculationSlug);
  if (!calculation) {
    onError("Data returned from server in redux is invalid");
    return undefined;
  }
  return calculation;
}

/** Calculation page. */
function CalculationView({match, categories}) {
  const [calculation, setCalculation] = useState(undefined);
  const [currentPayload, setCurrentPayload] = useState(undefined);
  const [error, setError] = useState(undefined);
  const [answer, setAnswer] = useState(undefined);
  const [loadingAnswer, setLoadingAnswer] = useState(false);

  const classes = useStyles();

  const onError = message => setError({message});

  useEffect(() => {
    const categoriesError = categories.error;
    if (categoriesError) {
      setError(categoriesError);
      return;
    }

    const {categorySlug, calculationSlug} = match.params;
    const calc = processCalculationProps(categories.data, categorySlug, calculationSlug, onError);
    if (calc) {
      setCalculation(calc);
    }
  }, [categories]);

  useEffect(() => {
    return () => {
      setLoadingAnswer(false);
    };
  }, [answer, error]);

  const onSubmitInputForm = (event, calculationSlug) => {
    event.preventDefault();
    setLoadingAnswer(true);

    const form = event.target;

    const mathPayload = createMathPayload(form, onError);
    if (currentPayload === mathPayload) {
      setLoadingAnswer(false);
      return; // same input was given
    }
    setCurrentPayload(mathPayload);
    if (mathPayload) {
      getCalculationResult(calculationSlug, mathPayload)
        .then(resp => {
          const toJSON = (resp, isErr = false) =>
            resp
              .json()
              .then(data => {
                if (isErr) {
                  setAnswer(undefined)
                  setError({message: data.error});
                } else {
                  setError(undefined)
                  setAnswer({content: data.answer});
                }
              })
              .catch(() => {
                setError({message: "Failed to serialise JSON data returned from server."});
              });

          switch (resp.status) {
            case 200:
              toJSON(resp);
              break;
            case 400:
              toJSON(resp, true);
              break;
            case 429:
              setError({
                message: "You sent too many requests to the server, come back tomorrow.",
              });
              break;
            case 500:
              toJSON(resp, true);
              break;
            default:
              setError({message: "The server has encountered a problem."});
          }
        })
        .catch(err => {
          setError({message: err.message});
        });
    }
  };

  if (!calculation && categories.error) {
    return <ErrorView error={categories.error}/>;
  }
  if (!calculation) {
    return <LoadingSpinner/>;
  }

  return (
    <>
      <DocumentTitle>{`${calculation.name} - MathFever`}</DocumentTitle>
      <MetaDescription>{calculation.description}</MetaDescription>

      <Fab
        variant="extended"
        size="medium"
        color="primary"
        disableRipple
        aria-label={calculation.category}
        className={classes.categoryHeading}
        component={Link}
        to={`/category/${calculation.categorySlug}`}>
        {calculation.category}
      </Fab>
      <h2 className={classes.calcHeading}>{calculation.name}</h2>

      <CalculationInputForm
        calculation={calculation}
        onSubmit={e => onSubmitInputForm(e, calculation.slug)}
      />
      <Divider className={classes.separator} light/>
      {loadingAnswer ? (
        <LoadingSpinner/>
      ) : (
          <Slide direction="up" in={answer || error ? true : false}>
            <AnswerContentCard
              answer={answer ? answer.content : undefined}
              error={error ? error.message : undefined}
            />
          </Slide>
      )}
    </>
  );
}

const mapStateToProps = state => {
  return {
    categories: state.categories,
  };
};

export default connect(mapStateToProps)(CalculationView);
