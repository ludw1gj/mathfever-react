import React, {useEffect, useState} from "react";
import {connect} from "react-redux";

import DocumentTitle from "../components/generic/DocumentTitle";
import LoadingSpinner from "../components/generic/LoadingSpinner";
import CategoryContent from "../components/CategoryContent";
import ErrorView from "../views/ErrorView";
import {deriveCategory} from "../services/util";

function processCategoryFromProps(categoriesData, categorySlug, onError) {
  if (!categoriesData) {
    return undefined;
  }
  const category = deriveCategory(categoriesData, categorySlug);
  if (!category) {
    onError("Data returned from server in redux is invalid");
  } else {
    return category;
  }
}

/** Category page. */
function CategoryView({categories, match}) {
  const [category, setCategory] = useState(undefined);
  const [error, setError] = useState(undefined);

  useEffect(() => {
    const categoriesError = categories.error;
    if (categoriesError) {
      setError(categoriesError);
      return;
    }

    const onError = err => setError(err);
    const {categorySlug} = match.params;
    const _category = processCategoryFromProps(categories.data, categorySlug, onError);
    if (_category) {
      setCategory(_category);
    }
  }, [categories]);

  if (!category && error) {
    return <ErrorView error={error}/>;
  }
  return (
    <>
      <DocumentTitle>
        {category ? `${category.name} - MathFever` : "Category - MathFever"}
      </DocumentTitle>

      {category ? <CategoryContent category={category}/> : <LoadingSpinner/>}
    </>
  );
}

const mapStateToProps = state => {
  return {
    categories: state.categories,
  };
};

export default connect(mapStateToProps)(CategoryView);
