import React from "react";

import DocumentTitle from "../components/generic/DocumentTitle";
import PaperSheet from "../components/generic/PaperSheet";
import ErrorImage from "../components/generic/ErrorImage";
import GenericDocMetaDesc from "../components/GenericDocMetaDesc";
import panda from "../assets/images/500-panda.jpg";

/** Error page. */
function ErrorView({error}) {
  return (
    <>
      <DocumentTitle>Error - MathFever</DocumentTitle>
      <GenericDocMetaDesc/>

      <PaperSheet>
        <h2>Error &rarr; Something went wrong.</h2>
        <p>
          <b>{error}</b>
        </p>
        <h4>Here's a panda:</h4>
        <ErrorImage src={panda} alt="Error 500"/>
      </PaperSheet>
    </>
  );
}

export default ErrorView;
