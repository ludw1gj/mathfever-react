import React from "react";
import {Typography} from "@material-ui/core";
import {Link} from "react-router-dom";

import DocumentTitle from "../components/generic/DocumentTitle";
import PaperSheet from "../components/generic/PaperSheet";
import PageHeading from "../components/generic/PageHeading";
import GenericDocMetaDesc from "../components/GenericDocMetaDesc";

const styles = {
  link: {
    color: "rgb(255,64,129)",
  },
};

/** Help page. */
function HelpView() {
  return (
    <>
      <DocumentTitle>Help - MathFever</DocumentTitle>
      <GenericDocMetaDesc/>

      <PageHeading>Help</PageHeading>

      <PaperSheet>
        <Typography variant="h5" component="h3" gutterBottom>
          <strong>Found any mistakes?</strong>
        </Typography>
        <Typography paragraph>
          If you find any mistakes post a comment on the{" "}
          <Link style={styles.link} to={"/message-board"}>
            message board.
          </Link>{" "}
          detailing the issue.
        </Typography>

        <Typography variant="h5" component="h3" gutterBottom>
          <strong>Want to request a feature?</strong>
        </Typography>

        <Typography paragraph>
          Post it on the{" "}
          <Link style={styles.link} to={"/message-board"}>
            message board.
          </Link>{" "}
          _へ__(‾◡◝ )>
        </Typography>

        <Typography variant="h5" component="h3" gutterBottom>
          <strong>Do you want to ask a question?</strong>
        </Typography>

        <Typography paragraph>
          Just ask on the{" "}
          <Link style={styles.link} to={"/message-board"}>
            message board.
          </Link>
        </Typography>

        <Typography variant="h5" component="h3" gutterBottom>
          <strong>Got mad advice for the admin?</strong>
        </Typography>

        <Typography paragraph>
          →{" "}
          <Link style={styles.link} to={"/message-board"}>
            message board.
          </Link>{" "}
          ←
        </Typography>
      </PaperSheet>
    </>
  );
}

export default HelpView;
