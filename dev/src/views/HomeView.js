import React, {useEffect, useState} from "react";
import {connect} from "react-redux";
import {Grid} from "@material-ui/core";
import {makeStyles} from "@material-ui/styles";

import DocumentTitle from "../components/generic/DocumentTitle";
import PageHeading from "../components/generic/PageHeading";
import LoadingSpinner from "../components/generic/LoadingSpinner";
import GenericDocMetaDesc from "../components/GenericDocMetaDesc";
import CategoryCard from "../components/CategoryCard";
import ErrorView from "./ErrorView";

import networkingImage from "../assets/images/category/networking.jpg";
import primesAndFactorsImage from "../assets/images/category/primes-and-factors.jpg";
import percentagesImage from "../assets/images/category/percentages.jpg";
import totalSurfaceAreaImage from "../assets/images/category/total-surface-area.jpg";

const IMAGES = new Map([
  ["networking", networkingImage],
  ["primes-and-factors", primesAndFactorsImage],
  ["percentages", percentagesImage],
  ["total-surface-area", totalSurfaceAreaImage],
]);

const useStyles = makeStyles({
  root: {
    flexGrow: 1,
  },
});

/** Home page. */
function HomeView(props) {
  const [categories, setCategories] = useState(undefined);
  const classes = useStyles();

  useEffect(() => {
    setCategories(props.categories.data);
  }, [props.categories]);

  if (props.categories.error) {
    return <ErrorView error={props.categories.error}/>;
  }
  return (
    <>
      <DocumentTitle>MathFever</DocumentTitle>
      <GenericDocMetaDesc/>

      <PageHeading>Home</PageHeading>

      <div className={classes.root}>
        <Grid container spacing={3}>
          {categories ? (
            categories.map(category => {
              return (
                <Grid key={category.slug} item xs>
                  <CategoryCard image={IMAGES.get(category.slug)} category={category}/>
                </Grid>
              );
            })
          ) : (
            <LoadingSpinner/>
          )}
        </Grid>
      </div>
    </>
  );
}

const mapStateToProps = state => {
  return {
    categories: state.categories,
  };
};

export default connect(mapStateToProps)(HomeView);
