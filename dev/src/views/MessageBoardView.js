import React from "react";
import {Typography} from "@material-ui/core";

import DocumentTitle from "../components/generic/DocumentTitle";
import PaperSheet from "../components/generic/PaperSheet";
import PageHeading from "../components/generic/PageHeading";
import GenericDocMetaDesc from "../components/GenericDocMetaDesc";

import cat from "../assets/images/first-post.jpg";

const RoundImage = ({alt, ...rest}) => {
  return <img {...rest} alt={alt} style={{maxWidth: "100%", borderRadius: "15px"}}/>;
};

/** Message Board page. */
function MessageBoardView() {
  return (
    <>
      <DocumentTitle>Message Board - MathFever</DocumentTitle>
      <GenericDocMetaDesc/>

      <PageHeading>Message Board</PageHeading>

      <PaperSheet>
        <Typography variant="h4" component="h3" gutterBottom>
          A Warm Welcome To This Slightly Terrible Site!
        </Typography>
        <Typography paragraph>
          May it be slightly terrible or just plain terrible, if at least someone finds this site
          useful I'll be as happy as a clam.
        </Typography>
        <Typography paragraph>What's a clam, here's a cat:</Typography>

        <RoundImage src={cat} width="400" alt="cat"/>
        <Typography paragraph>
          <strong>Nov. 19, 2016</strong>
        </Typography>
        <Typography paragraph>Page 1 of 1</Typography>
        <Typography color="error" style={{fontWeight: "bold"}}>
          NOTE: This page's functionality was stripped when migrating from the Django implementation
          of this web application.
        </Typography>
      </PaperSheet>
    </>
  );
}

export default MessageBoardView;
