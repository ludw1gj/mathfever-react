import React from "react";

import DocumentTitle from "../components/generic/DocumentTitle";
import ErrorImage from "../components/generic/ErrorImage";
import PaperSheet from "../components/generic/PaperSheet";
import GenericDocMetaDesc from "../components/GenericDocMetaDesc";

import goat from "../assets/images/404-goat.jpg";

/** Not Found page. */
function NotFoundView() {
  return (
    <>
      <DocumentTitle>Page Not Found - MathFever</DocumentTitle>
      <GenericDocMetaDesc/>

      <PaperSheet>
        <h2>Error 404 &rarr; This page doesn't exist.</h2>
        <h4>There's nothing here but us.</h4>
        <ErrorImage src={goat} alt="Error 404"/>
      </PaperSheet>
    </>
  );
}

export default NotFoundView;
